var gulp        = require('gulp');
var sass        = require('gulp-sass');
//group file to template
var concat = require('gulp-concat');
//min css
var cssmin		= require('gulp-cssmin');
var template = require('gulp-template-html');
var rename = require("gulp-rename");

// complie *.scss
gulp.task('mergehtml', function() {
  return gulp.src('src/include/*.html')
    .pipe(concat('all.html'))
    .pipe(gulp.dest('src/templates'));
});
//sass developer
gulp.task('sassdev', () => {
	return gulp .src('src/assets/css/*.scss')
		   		.pipe(sass().on('error', sass.logError))
		   		//.pipe(cssmin())/* lenh min css co the bo qua*/
		   		.pipe(gulp.dest('dev/assets/css'))
		   		//.pipe(reload({stream:true}));
});
//sass product
gulp.task('sass', () => {
	return gulp .src('src/assets/css/*.scss')
		   		.pipe(sass().on('error', sass.logError))
		   		.pipe(cssmin())/* lenh min css co the bo qua*/
		   		.pipe(gulp.dest('build/assets/css'))
		   		//.pipe(reload({stream:true}));
});
gulp.task('template', function () {
	const glob = require('glob');

	const fileArray = glob.sync('src/*.*');
	for(let i =0,len=fileArray.length; i<len;i++){
		let filename=fileArray[i].replace('src/','');
		
		gulp.src('src/templates/all.html')
		      .pipe(template('src/'+filename))
		      .pipe(rename(function (path) {
			    // Returns a completely new object, make sure you return all keys needed!
			    return {
			      dirname: path.dirname + "",
			      basename:filename,
			      extname: ""
			    };
			  }))
		      .pipe(gulp.dest('build'));
	}
	

    
});